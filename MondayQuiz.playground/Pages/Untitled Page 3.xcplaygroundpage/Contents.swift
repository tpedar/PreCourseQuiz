//: [Previous](@previous)
/*:

## Making a Class

Create a new Class called Car. Give Car the following properties: make: String, model: String, year: Int, topSpeed: Int, and isStreetLegal: Bool optional.

Make an init method and initalize your class.

Print all of the properties. Unwrap the isStreetLegal optional. If it is street legal: print "Good to go". If it is not street legal: print "Pull over". If it is nil: print "Haven't checked"
*/

import Foundation


class Car {
    
    let make: String
    let model: String
    let year: Int
    let topSpeed: Int
    let isStreetLegal: Bool?
    
    init(makeInput: String, modelInput: String, yearInput: Int, topSpeedInput: Int, isStreetLegalInput: Bool) {
        self.make = makeInput
        self.model = modelInput
        self.year = yearInput
        self.topSpeed = topSpeedInput
        self.isStreetLegal = isStreetLegalInput
    }
}

let car1 = Car(makeInput: "Honda", modelInput: "Civic", yearInput: 2012, topSpeedInput: 10, isStreetLegalInput: true)

if let legal = car1.isStreetLegal {
    if legal == true {
        print("Good to go")
    } else if legal == false {
        print("Pull Over")
    }
} else {
    print("Haven't Checked")
}
//: [Previous](@previous)
//: [Next](@next)
